﻿/*
 * extend.js JavaScript library - v. 0.1
 * https://bitbucket.org/christofereliasson/extend.js/overview
 */

(function (undefined) {

    /* ### Exceptions ### */
    var exception = {
        invalidInputNumber: function (i) {
            throw new Error(i + " is not a valid number.");
        },
        notPositiveNumber: function (i) {
            throw new Error(i + " is not a positive number.");
        }
    };

    /* ### String extensions ### */

    /*
    * Trim-method that handles even long string fast in all browsers
    * Source: http://blog.stevenlevithan.com/archives/faster-trim-javascript
    */
    String.prototype.trim = function () {
        str = this.replace(/^\s+/, '');
        for (var i = str.length - 1; i >= 0; i--) {
            if (/\S/.test(str.charAt(i))) {
                str = str.substring(0, i + 1);
                break;
            }
        }
        return str;
    };

    /*
    * Return the first x characters of a string
    */
    String.prototype.left = function left(num) {
        if (typeof num !== "number") {
            throw exception.invalidInputNumber(num);
        }
        if (num < 1) {
            throw exception.notPositiveNumber(num);
        }
        return this.substring(0, num);
    };

    /*
    * Return the last x characters of a string
    */
    String.prototype.right = function (num) {
        var end = this.length;
        if (typeof num !== "number") {
            throw exception.invalidInputNumber(num);
        }
        if (num < 1) {
            throw exception.notPositiveNumber(num);
        }
        return this.substring(end - num, end);
    };

})();